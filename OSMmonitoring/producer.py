import requests
import sched, time
from fastcore.xtras import Path
from json import dumps
from kafka import KafkaProducer
from xml.etree.ElementTree import fromstring
from xmljson import badgerfish as bf

s = sched.scheduler(time.time, time.sleep)

# Create Producer
producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                         value_serializer=lambda x:
                         dumps(x).encode('utf-8'),
                         acks=1,
                         max_request_size=1000000000,
                         buffer_memory=1000000000,
                         send_buffer_bytes=1000000000)


def call_api(prev_status):
    # Get current seq nr
    status = requests.get('https://overpass-api.de/api/augmented_diff_status')

    status = int(status.text)
    old_status = int(prev_status)

    if status != old_status:
        print('Diff will be requested with id: ', status)

        url = 'https://overpass-api.de/api/augmented_diff?id=' + str(status)

        # Read content
        data = requests.get(url)
        data = dumps(bf.data(fromstring(data.content)))

        # Send message and print answer
        future = producer.send('osm_diff', data)
        # print('future.get(): ', future.get())

        # Define output path
        out_path = Path(r'/home/kevin/Documents/OSMmonitor/output/output_producer')
        file_name = 'message_' + str(len(out_path.ls()) + 1) + '.txt'
        out_path = out_path / file_name
        # Write message to output file (just for testing purpose)
        output_file = open(out_path, 'w')
        output_file.write(str(status))
        output_file.write(data)
        output_file.close()
    else:
        print('No differences detected!')

    s.enter(40, 1, call_api, (status,))


print('Calling API')
s.enter(40, 1, call_api, (0,))
s.run()
