from pathlib import Path
import pandas as pd

pd.set_option('display.max_columns', 200)
pd.set_option('display.width', 2000)


# Initialize global variable
create = False
wikidata = False
compare_wiki = dict()


def extract_information(action, message):
	def unpack(msg):
		csv_entry = [msg['@id'], msg['@version'], msg['@timestamp'], msg['@changeset'], msg['@user'], action]
		return csv_entry

	if action == 'create':
		return unpack(message)
	else:  # modify,insert
		for k, v in message.items():
			element = k
			return unpack(v)


def parse_dict(input_dict):
	global status;
	global action;
	global create;
	global wikidata;
	global chg_n;
	global df_chg;

	def compare_wiki_value():
		if status == 'old':
			compare_wiki['old'] = value
		elif status == 'new':  # 'create' has not status
			compare_wiki['new'] = value
			if compare_wiki['old'] != compare_wiki['new']:
				# chg_n.append(compare_wiki['new'])
				chg_n.insert(5, compare_wiki['new'])
				df_chg.loc[len(df_chg)] = chg_n

		else:
			chg_n.append(value)
			df_chg.loc[len(df_chg)] = chg_n

	def parse_list(diff_list):
		for list_item in diff_list:
			if type(list_item) is dict:  # isinstanceof
				parse_dict(input_dict=list_item)
			elif type(list_item) is list:
				parse_list(diff_list=list_item)

	for key, value in input_dict.items():
		if value in ['delete','modify']:
			action = value
		if 'old' in key and 'wikidata' in str(value):
			status = 'old'
		if 'new' in key and 'wikidata' in str(value):
			chg_n = extract_information(action=action, message=value)
			status = 'new'
		if create and 'wikidata' in str(value):  # no old/new for create
			chg_n = extract_information(action=action, message=value)
			create = False  # next item won't be dict of create
		if value == 'create':
			create = True  # next item is dict of create
			action = value
		else:
			create = False

		if type(value) is dict:
			parse_dict(input_dict=value)
		elif type(value) is list:
			parse_list(diff_list=value)
		elif type(value) in (str, int, float, bool):
			if wikidata:
				compare_wiki_value()
				wikidata = False  # next item will not be wikidata value again
			if value == 'wikidata':
				wikidata = True  # next item (value) will be wikidata value


def monitor_diff_file(msg):
	global df_chg
	df_chg = pd.DataFrame(columns=['osm_id', 'osm_version', 'timestamp', 'changeset_id', 'user', 'wikidata_tags_list', 'action'])
	parse_dict(input_dict=msg)
	if not df_chg.empty:
		if Path(r'./wikidata_diff.csv').is_file():
			df_wikidata_diff_csv = pd.read_csv(r'./wikidata_diff.csv')
			df_wikidata_diff_csv = df_wikidata_diff_csv.append(df_chg)
			df_wikidata_diff_csv.to_csv(path_or_buf=r'./wikidata_diff.csv', index=False)
		else:
			df_chg.to_csv(r'./wikidata_diff.csv',index=False)