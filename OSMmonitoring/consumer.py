import pandas as pd
from kafka import KafkaConsumer
import json
import monitor

consumer = KafkaConsumer(
	'osm_diff',
	bootstrap_servers=['localhost:9092'],
	auto_offset_reset='earliest',
	enable_auto_commit=True,
	group_id=None,
	fetch_max_bytes=100000000,
)
counter = 1
for message in consumer:
	message = message.value.decode('utf-8')
	message_dict = json.loads(json.loads(message))
	print('msg_nr:', counter)
	monitor.monitor_diff_file(message_dict)
	counter +=1

