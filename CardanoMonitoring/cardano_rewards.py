from blockfrost import BlockFrostApi, ApiError, ApiUrls
import pandas as pd

api = BlockFrostApi(
    project_id='mainnetFkrryK1oS9SUHKkN7hGRnuckpSoGSGvt',
    base_url=ApiUrls.mainnet.value
)

try:

    account_rewards = api.account_rewards(
        # stake_address='stake1u96fay77wx4kfwy4msqecaagmcjxnncnuet9x86rfhzqjmcgnpljj',
        stake_address='stake1uyuph0ktrtt92gxw9z6t9uquvavwn229y7hajlea48ymdvgzzv65z',
    count=100,
    )

    rewards = 0
    for count in range(len(account_rewards)):
        print('Epoch: ', account_rewards[count].epoch, '-> ', int(account_rewards[count].amount) / 1000000, 'ADA')
        rewards += int(account_rewards[count].amount) / 1000000
    print('Total epoch: ', len(account_rewards), 'Total Rewards: ', rewards, ' ADA')

except ApiError as e:
    print(e)
