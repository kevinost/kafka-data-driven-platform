import smtplib, ssl
import pandas as pd
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


port = 465  # For SSL
password = 'YOUR PASSWORD'
sender_email = 'YOUR SENDER ADDRESS'
receiver_email = "YOUR RECEIVER ADDRESS"

pd.set_option('display.max_colwidth', None)
pd.set_option('display.max_rows', 200)
pd.set_option('display.max_columns', 200)
pd.set_option('display.width', 2000)


def convert_message(msg):
    """
    Initializes mail and converts given message int dataframe / html
    """
    message = MIMEMultipart("alternative")
    message["Subject"] = "ADA SUISSE UPDATE"
    message["From"] = sender_email
    message["To"] = receiver_email
    cols = ['stake_address', 'stake_amount']

    for m in msg:
        if m == 'LOST':
            message_status = 'Unfortunately, delegators have left. \n\n '
            message_status_html = '<p>Unfortunately, delegators have left.</p>'
        elif m == 'CHANGE':
            message_status = 'Amounts have changes! \n\n '
            cols = ['stake_address', 'stake_amount (before --> after)']
            message_status_html = '<p> Amounts changed! </p>'
        elif m == 'NEW':
            message_status = 'New delegators have joined your pool! \n\n '
            message_status_html = '<p> New delegators have joined your pool! </p>'

        # Create Dataframe out of dict
        df_msg = pd.DataFrame.from_dict(data=msg.get(m), orient='index')
        df_msg = df_msg.set_axis([cols],axis=1)
        # Create message out of DataFrame
        df_msg_str = df_msg.to_string(header=cols, justify='left', index=False)
        df_msg_html = df_msg.to_html(header=cols, justify='left', index=False)

        message_plain = 'Dear SPO' + '\n' + message_status + df_msg_str
        message_html = '<p>Dear SPO</p>' + message_status_html + df_msg_html

        # Turn these into plain/html MIMEText objects
        part1 = MIMEText(message_plain, "plain")
        part2 = MIMEText(message_html, "html")

        # Add HTML/plain-text parts to MIMEMultipart message
        message.attach(part1)
        message.attach(part2)

    return message


def send_message(msg):
    # Create a secure SSL context
    context = ssl.create_default_context()

    with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
        server.login("adasuisse.update.service@gmail.com", password)
        server.sendmail(sender_email, receiver_email, msg.as_string())

