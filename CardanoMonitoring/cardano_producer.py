import pandas as pd
import numpy as np
from blockfrost import BlockFrostApi, ApiError, ApiUrls
import sched, time
import json
from kafka import KafkaProducer

pd.set_option('display.max_colwidth', None)
pd.set_option('display.max_rows', 200)
pd.set_option('display.max_columns', 200)
pd.set_option('display.width', 2000)

s = sched.scheduler(time.time, time.sleep)

# Create Producer
producer = KafkaProducer(bootstrap_servers=['localhost:9092'],
                         value_serializer=lambda x:
                         json.dumps(x).encode('utf-8'),
                         acks=1,
                         max_request_size=1000000000,
                         buffer_memory=1000000000,
                         send_buffer_bytes=1000000000)

counter = 1
def call_api(df_delegator_status):
    global counter; counter +=1
    def get_standalone(df_previous, df_current):
        # Join the dataframes (left and right) and indicate the appearance in the last column
        df_delegator_status_previous_all = df_current.merge(df_previous, how='right', on=unique_col, indicator=True)  # RIGHT JOIN -> ALL PREVIOUS
        df_delegator_status_current_all = df_current.merge(df_previous, how='left', on=unique_col, indicator=True)  # LEFT JOIN -> ALL CURRENT

        # Show standalone rows
        df_delegator_status_previous_sa = df_delegator_status_previous_all[df_delegator_status_previous_all['_merge'] == 'right_only']
        df_delegator_status_current_sa = df_delegator_status_current_all[df_delegator_status_current_all['_merge'] == 'left_only']
        return df_delegator_status_previous_sa,df_delegator_status_current_sa

    def drop_standalones(sa_previous, sa_current):
        # Previous
        if not sa_previous.empty:
            print('LOST DELEGATORS:')
            df_lost_delegator = sa_previous.drop(columns=['stake_amt_x','_merge'])
            print(df_lost_delegator)
            send_message('LOST',df_lost_delegator)
            # Drop lost delegators for comparison
            for i in sa_previous[unique_col]:
                df_delegator_status_previous.drop(df_delegator_status_previous[df_delegator_status_previous[unique_col] == i].index, inplace=True)

        # Current
        if not sa_current.empty:
            print('NEW DELEGATORS:')
            df_new_delegator = sa_current.iloc[:,:2]
            print(df_new_delegator)
            send_message('NEW', df_new_delegator)
            # Drop new delegators for comparison
            for i in sa_current[unique_col]:
                df_delegator_status_current.drop(df_delegator_status_current[df_delegator_status_current[unique_col] == i].index, inplace=True)

        # Check if both reports equal rows
        if len(df_delegator_status_previous.index) == len(df_delegator_status_current.index):
            return df_delegator_status_previous.sort_values(by=[unique_col]), df_delegator_status_current.sort_values(by=[unique_col])
        else:
            print('Shape of df_previous is: ', df_delegator_status_previous.shape)
            print('Shape of df_current is: ', df_delegator_status_current.shape)
            print('log: Alignment did not work properly. Check function creates_comparison_files..')
            return None

    def get_differences(df_previous, df_current):
        # Compare entire DF - set TRUE /FALSE to each field
        comparison_values = df_current.values == df_previous.values # False

        # Get coordination of fields containing FALSE
        rows, cols = np.where(comparison_values == False)

        for item in zip(rows, cols):
            if (str(df_current.iloc[item[0], item[1]]) == 'nan') and (str(df_previous.iloc[item[0], item[1]]) == 'nan'):
                df_current.iloc[item[0], item[1]] = ''
            else:
                df_current.iloc[item[0], item[1]] = '{} --> {}'.format(df_previous.iloc[item[0], item[1]],df_current.iloc[item[0], item[1]])

        return filter_dataframe(columns=df_current.columns, df_current=df_current.astype('string'))

    def filter_dataframe(columns, df_current=None):
        """
        Filters for all rows containing an arrow (-->) which indicates that the row contains a difference
        """
        filter_str = ''
        cnt = 0

        for col in columns:
            # Only select rows which have difference in at least on column
            if cnt < len(columns) - 1:
                filter_str += "df_current['" + col + "'].str.contains('-->') | "
            else:
                filter_str += "df_current['" + col + "'].str.contains('-->')]"
            cnt += 1

        filter_str = 'df_current[' + filter_str

        df_differences = eval(filter_str)

        if not df_differences.empty:
            print('AMOUNT CHANGED FOR: \n ', df_differences)
            send_message('CHANGE', df_differences)
            print('log: Comparison finished!')

            return df_differences
        else:
            print('#', counter,': No differences detected!!')
            return df_differences

    def send_message(status, df_chg):
        send_dict = dict()
        send_dict[status] = {}

        dict_file = df_chg.to_json(orient='index')
        dict_file = json.loads(dict_file)
        send_dict[status] = dict_file
        future = producer.send('delegator_update', str(send_dict))

    # Given project_id from API registration
    api = BlockFrostApi(
        project_id='mainnetFkrryK1oS9SUHKkN7hGRnuckpSoGSGvt',
        base_url=ApiUrls.mainnet.value
        )

    # Get delegator's address and amount
    delegator_amt = {}
    unique_col = 'address'
    try:
        pool_updates = api.pool_delegators(
            pool_id='pool1w9yeaaadjz9563tf2h7katrlujvxnywda8el8z66h02ckccx95x',
            count=100,
            order='desc',
            gather_pages=True
        )
        for count, update in enumerate(pool_updates):
            delegator_amt[count] = [update.address, int(update.live_stake)/1000000]

        df_delegator_status_previous = df_delegator_status
        df_delegator_status_current = pd.DataFrame.from_dict(data=delegator_amt, orient='index', columns=['address','stake_amt'])
        # Copy since current status will be used for next time
        df_status_current = df_delegator_status_current.copy()

        # Preprocess and comparison
        df_sa_previous, df_sa_current = get_standalone(df_delegator_status_previous, df_delegator_status_current)
        df_previous, df_current = drop_standalones(df_sa_previous,df_sa_current)
        get_differences(df_previous,df_current)

    except ApiError as e:
        print(e)

    s.enter(60, 1, call_api, (df_status_current,))


print('Calling API')
s.enter(60, 1, call_api, (pd.DataFrame(columns=['address', 'stake_amt']),))
s.run()

"""
test_dict = {
0:['stake1uy7thhrq7nxjugfq9esyu5wqvpj4lng4wru563ccefszh2g4qlee2',   85340.385659],
1:['stake1u8aqf6s5g8r92kg83lxuuqarlgfp5fpvslvm0cy60a5dz7c6umwau',   50967.116763],
2:['stake1u9ch6g79244zqk99xhv0a4erzeasga0wz6nj536h9w0ez4g8hz4lm',   13752.825567],
3:['stake1uyu0fazmhzzedasdlua528etqcjqr6sde40yysxverpmpxq4vyrsn',     477.825743],
4:['stake1uy5cdp44608u56dt50gpvwh4v3a0tc6gwgzuqac6cfs4jxspwmkz3',    4969.352275],
5:['stake1uy8xg5f7qv2t78ncxzyxny9056yh467tn5lcrhf8l67hn2ssu2pqq',     597.245188]
}

df_test = pd.DataFrame.from_dict(data=test_dict, orient='index',columns=['address','stake_amt'])
call_api(pd.DataFrame(df_test))
"""