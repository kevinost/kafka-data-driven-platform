import pandas as pd
from kafka import KafkaConsumer
import json
import ast
import cardano_mailing as msgout

consumer = KafkaConsumer(
	'delegator_update',
	bootstrap_servers=['localhost:9092'],
	auto_offset_reset='earliest',
	enable_auto_commit=True,
	group_id=None,
	fetch_max_bytes=100000000,
)
counter = 1
for message in consumer:
	print('msg_nr:', counter)
	message = message.value.decode('utf-8')
	message = message.replace("'",'"')
	message_dict = json.loads(message.strip('"'))
	msg = msgout.convert_message(message_dict)
	msgout.send_message(msg)
	counter += 1