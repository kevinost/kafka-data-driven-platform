# Kafka-Data-Driven-Platform  
In this repository two use cases are described which require the installation of Apache Kafka and Python


<img src="img/Use_Cases.png"  width="530" height="210">  

### Business Use Case 1: OSMmonitoring
The first use case is about examining changes in OpenStreetMap in real time for specific content  and deriving an action from that. The [Overpass API/Augmented Diffs](https://wiki.openstreetmap.org/wiki/Overpass_API/Augmented_Diffs) service was used as the data source. Augmented diff offers a comparison of the result of the same query in two different times. The final result contains all the data of nodes, ways and relations with difference between the starting time and the ending time. 
The extracted differences are transformed into a JSON format in a first step and then published to the Kafka cluster via the kafka-python API. Once the data arrives in the topic "osm_diff", it is consumed by a monitoring tool. The tool “OSMMonitoring” was implemented in the context of this thesis and examines whether a changeset changes a wikitag. If this is the case, the new value and further information is written to a CSV file.

### Business Use Case 2: CardanoMonitoring
This use case allows a cardano pool operator to receive email notifications when the delegated amount in the specified pool has changed. This occurs when either an existing delegator changes (reduces/increases) its stake amount or when a new delegator delegates his/her stake into the pool. 
The get extracted using the [Blockfrost API](https://blockfrost.io/). After registering at Blockfrost, the Cardano Blockchain can be accessed with the received project_id. The data is returned either as JSON objects or arrays. 
For this use case, the addresses and stake amount of all delegators of a given Cardano pool are queried every minute. Since the pool operator should only be informed if the amount of delegators or the stake amount has changed, the analysis tool "CardanoMonitoring" compares the results with each other and  published the differences to the Kafka topic called “delegator_update”.  


## Installation
* Install Kafka on your machine (preferably linux)
* Pull the repository
* Replace the project_id in the file cardano_producer.py with your Blockfrost [project_id](https://docs.blockfrost.io/)
* Replace sender_email, receiver_email and password in the file cardano_mailing.py


## Usage
* Open Shell and start ZooKeeper
* Open Shell and start Kafka broker service 
* Open Shell and create the topics osm_diff (UseCase1) and delegator_update (UseCase2)
* run cardano_producer.py / cardano_consumer.py (UseCase1)
* run cardano.py / producer.py (UseCase2)

#### Kafka commands (linux)
* Start the ZooKeeper service:  
$ bin/zookeeper-server-start.sh config/zookeeper.properties

* Start the Kafka broker service:  
$ bin/kafka-server-start.sh config/server.properties

* create topic:  
$ bin/kafka-topics.sh --create --topic yourtopic --bootstrap-server localhost:9092

* describe topic:  
$ bin/kafka-topics.sh --describe --topic yourtopic --bootstrap-server localhost:9092

* write event into topic:  
$ bin/kafka-console-producer.sh --topic yourtopic --bootstrap-server localhost:9092

* read event:  
$ bin/kafka-console-consumer.sh --topic yourtopic --from-beginning --bootstrap-server localhost:9092




## Author
------
Tool is written by [Kevin Ammann](https://gitlab.com/kevinost).


Feedback is welcome.



